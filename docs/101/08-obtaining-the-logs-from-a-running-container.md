# Obtaining the logs from a running container

Running applications in containers is great but we need a way of obtaining the logs from them.

In this lab we will demonstrate how to obtain the logs from a running container.

## Prerequisites

For this session you will need to browse to the `demos\build` directory (see below).

```
cd demos/logs
```

If you view the Dockerfile by exeucting the following command:

```
less Dockerfile
```

The following should be displayed:

```
FROM quay.io/apprenda/alpine
MAINTAINER Steve Wade <swade@apprenda.com>
CMD ["bash", "-c", "while true; do echo Welcome to Docker 101; sleep 5; done"]
```

This container simply prints "Welcome to Docker 101" to STDOUT every 5 seconds.


## 1. Build a new docker image

From the logs directory execute the following command:

```
docker build --no-cache=true -t logging-container .
```

## 2. Run a container using the newly created image

To spin up a container using this image execute the following command:

```
docker run --name log-container -d logging-container
```

## 3. Obtain the logs from the container

To obtain the logs from the container simply execute the following command:

```
docker logs log-container
```

The following should be output to the screen:

```
Welcome to Docker 101
Welcome to Docker 101
```

If you keep execute the `docker logs` command you we see more output.