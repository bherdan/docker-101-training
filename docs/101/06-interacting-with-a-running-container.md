# Interacting with a running container

In the previous lab we finished with a docker container running.

In this lab we will demonstrate how to get inside the container whilst its running.

Although containers are meant to be idempotent sometimes you may want to get inside them to debug.


## Dockerfile reminder

Firstly we want to reminder ourselves about the construct of the container.
 
We can do this by checking out the Dockerfile.

```
FROM nginx:alpine
MAINTAINER Steve Wade <swade@apprenda.com>
COPY . /usr/share/nginx/html
```

As we can see from above our files are copied into `/usr/share/nginx/html`.


## Obtaining entry to the container

Firstly execute the following command to make sure our container is still running:

```
docker ps -a
```

You should see:

```
CONTAINER ID        IMAGE                               COMMAND                  CREATED             STATUS              PORTS                    NAMES
38ca06faf7f6        localhost:5000/my-container:1.2.3   "nginx -g 'daemon ..."   4 minutes ago       Up 4 minutes        0.0.0.0:9999->80/tcp     my-container
83c7f820be98        registry:2                          "/entrypoint.sh /e..."   15 minutes ago      Up 15 minutes       0.0.0.0:5000->5000/tcp   registry
```

We will now use the following command to get inside the container:

1. The `-it` means that we make the container interactive.
2. The `/bin/sh` is the command you want to execute inside the container. 

```
docker exec -it my-container /bin/sh
```

Note: If the container is paused, then the docker exec command will wait until the container is unpaused, and then run

## Taking a look inside

Now we are inside the container lets take a look around.

Execute the following commands:

```
cd /usr/share/nginx/html
ls -lart
```

You should now see something similar to:

```
-rw-r--r--    1 root     root           537 Nov 22 19:17 50x.html
drwxr-xr-x    1 root     root          4096 Nov 22 19:17 ..
-rw-r--r--    1 root     root            89 Nov 28 16:26 Dockerfile
-rw-r--r--    1 root     root           332 Nov 28 16:27 index.html
drwxr-xr-x    1 root     root          4096 Nov 28 18:59 .
```

This shows us the files which were uploaded to the container during the `docker build` command we ran earlier.

## Exiting the container

To exit the container simply execute `exit`