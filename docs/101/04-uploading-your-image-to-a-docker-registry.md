# Uploading your image to a docker registry

In the previous lab we created a tagged docker image with the name `my-container:1.2.3`

This image is currently in your local registry but we want to be able to push this a shared registry.


## Prerequisites

For this session you will need to browse to the `demos\build` directory (see below).

```
cd demos/build
```

## 1. Spinning up a local docker registry

Firstly we need to spin up a docker registry so we can push our image to it.

To do this simply execute the following command:

```
docker run -d -p 5000:5000 --name registry registry:2
```

We now have a docker registry running on [localhost:5000](localhost:5000)

## 2. Retagging our image appropriately

In order for our image to be uploaded to our registry we need to tag it appropriately.

So let's build a new docker image with the correct tag, to do this execute:

```
docker build --no-cache=true -t localhost:5000/my-container:1.2.3 .
```

Note that the `tag` flag above needs to be `<registry address>/<registry-repo>:<version>`

If we now execute the following command we will see our newly tagged image:

```
docker images
```

You should see the following:

```
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
localhost:5000/my-container   1.2.3               8ed7b98760f0        7 seconds ago       15.5MB
```

## 3. Pushing the image to your registry

Now we have an image tagged correctly so it is now time to push this to our registry.

To do this execute the following command:

```
docker push localhost:5000/my-container:1.2.3
```

## 4. Delete the local docker image 

Let's now delete the image locally and instead pull it from the local registry.

To perform this action execute the following command:

```
docker rmi localhost:5000/my-container:1.2.3
```

Something similar to the below should be output to the screen:

```
Untagged: localhost:5000/my-container:1.2.3
Untagged: localhost:5000/my-container@sha256:40174ae0c1000023c34c6658e2bb23f275d7078c50a3343cf4e668aeb3c3a992
Deleted: sha256:8ed7b98760f0a2d7ae969222c6e10a5f36db0c6b6e8acc10444040ca1ba8c2c0
Deleted: sha256:f30e37438ec6d590ea7abb3ab01c5e25fc295330373ad78871ad9341b578eb24
Deleted: sha256:1860894a754d298979d4acaf18201dded07248655e21f240db9c4a04008aa0a6
```

## 5. Pulling the image from our local registry

Now pull the image from the registry by executing the following command:

```
docker pull localhost:5000/my-container:1.2.3
```

Note: The image was pulled from the docker registry because we include the registry in the image now (see below):

```
e.g. localhost:5000
```