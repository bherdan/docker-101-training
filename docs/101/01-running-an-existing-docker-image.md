# Running an existing docker image

## Attached mode

The most basic way of running a docker container is running in a "attached" mode.

This means that the container is run in the background.

An example of this can be seen when executing the following docker command:

```
docker run hello-world
```

When running the command above, the container process executes and outputs the following:

```
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://cloud.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/engine/userguide/
```

After the process has exited the container is removed. 

## Detached mode

The more common way of running a docker container is in a detached mode.

This is where the container is span up and continues to run until either:

1. The container is manually stopped by the user.
2. The container process exits and stops the container from running.

We can demonstrate this by executing the command below:

```
docker run -d -p 8080:80 nginx:alpine
```

Now in your browser browse to [http://localhost:8080/](http://localhost:8080/).

You should see the following displayed

```
Welcome to nginx!

If you see this page, the nginx web server is successfully installed and working. 
Further configuration is required.

For online documentation and support please refer to nginx.org.
Commercial support is available at nginx.com.

Thank you for using nginx.
```

## Using the --restart flag

Docker also provides you the ability to set a restart policy when a container exits.

To demonstrate this execute the following command:

```
docker run --restart=always --name=restart-example -d busybox sleep 20
```

Now execute the following command:

```
watch docker ps -a
```

You will see the container exited after 20 seconds and restart automatically.