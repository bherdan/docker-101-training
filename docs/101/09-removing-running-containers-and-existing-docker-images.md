# Removing running containers and existing images

This lab will explain the steps required to delete existing images and containers running on docker.

## 1. Removing running containers

Firstly we need to obtain the list of running containers, to do so execute the following command:

```
docker ps -a
``` 

Something similar to the following should be output to the screen:

```
CONTAINER ID        IMAGE                               COMMAND                  CREATED              STATUS                     PORTS                    NAMES
eada86a18f43        logging-container                   "bash -c 'while tr..."   3 seconds ago        Up 3 seconds                                        log-container
3cae6a57b55f        localhost:5000/my-container:1.2.3   "nginx -g 'daemon ..."   About a minute ago   Up About a minute          0.0.0.0:9999->80/tcp     my-container
9e8b3b63f2eb        registry:2                          "/entrypoint.sh /e..."   3 minutes ago        Up 3 minutes               0.0.0.0:5000->5000/tcp   registry
b3566d65fcbe        busybox                             "sleep 20"               4 minutes ago        Up 13 seconds                                       restart-example
3be22c97240a        nginx:alpine                        "nginx -g 'daemon ..."   4 minutes ago        Up 4 minutes               0.0.0.0:8080->80/tcp     optimistic_shockley
0856f06a6d4b        hello-world                         "/hello"                 5 minutes ago        Exited (0) 5 minutes ago                            confident_poitras
```

Note: You are unable to delete docker images which are being used to run containers.

To delete the running containers we need to execute the following command:

```
docker rm -f <container name or container id>
```

In our example above that would mean the following command:

```
docker rm -f log-container my-container registry restart-example optimistic_shockley confident_poitras
```

If the deletion has been successful the following should be output to the screen:

```
log-container
my-container
registry
restart-example
optimistic_shockley
confident_poitras
```

## 2. Deleting docker images

We need to obtain the docker images to delete by executing the following command:

```
docker images
```

You should see the following list of images:

```
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
logging-container             latest              230540af1852        8 minutes ago       7.43MB
localhost:5000/my-container   1.2.3               e9fcfb378d4e        11 minutes ago      15.5MB
my-container                  1.2.3               04f7f4563f22        12 minutes ago      15.5MB
my-container                  latest              f19852818d6a        12 minutes ago      15.5MB
<none>                        <none>              b25cc9510408        12 minutes ago      15.5MB
nginx                         alpine              bf85f2b6bf52        6 days ago          15.5MB
hello-world                   latest              f2a91732366c        8 days ago          1.85kB
busybox                       latest              6ad733544a63        3 weeks ago         1.13MB
registry                      2                   a07e3f32a779        3 weeks ago         33.3MB
quay.io/apprenda/alpine       latest              ebbdd4366d5f        8 months ago        7.43MB
```

To delete the images we will be using the `docker rmi` command passing in the repository and tag.

To delete the images execute the following commands:

```
docker rmi logging-container 
docker rmi localhost:5000/my-container:1.2.3
docker rmi my-container
docker rmi my-container:1.2.3
docker rmi nginx:alpine
docker rmi hello-world
docker rmi busybox
docker rmi registry:2
docker rmi quay.io/apprenda/alpine
```